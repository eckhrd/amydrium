#-------------------------------------------------
#
# Project created by QtCreator 2016-05-03T17:16:21
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Amydrium
TEMPLATE = app
INCLUDEPATH += ./include


SOURCES +=\
    src/main.cpp \
    src/data.cpp \
    src/engine.cpp \
    src/mainwindow.cpp


HEADERS  +=\
    include/data.h\
    include/engine.h \
    include/mainwindow.h

FORMS    += \
    src/mainwindow.ui
