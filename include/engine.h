// This file is part of Amydrium.
//
// Amydrium is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Amydrium is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Amydrium.  If not, see <http://www.gnu.org/licenses/>.

#ifndef ENGINE_H
#define ENGINE_H

#include <vector>
#include "data.h"

#if defined __linux__ || defined __APPLE__
// "Compiled for Linux
#else
// Windows doesn't define these values by default, Linux does
#define M_PI 3.141592653589793
#define INFINITY 1e8
#endif

inline float mix(const float &a, const float &b, const float &mix) {
    return b * mix + a * (1 - mix);
}

class Engine {

public:

    Engine(bool refl = true, bool trans = true, unsigned rec = 5);

    Engine(const Engine& source_engine);

    Engine& operator = (const Engine& source_engine);

    ~Engine();

    Vec3* getImage(void);

    float render(const std::vector<Sphere> &spheres);

    void saveImage(void);

    void setTransparency(bool status);
    bool getTransparency(void);

    void setReflectivity(bool status);
    bool getReflectivity(void);

    void setRecursions(unsigned number_of_recursions);
    unsigned getRecursions();

private:

    bool reflectivity, transparency;
    unsigned recursions;
    float fov, angle;

    Vec3 *image, *pixel;
    unsigned width, height;
    float invWidth, invHeight, aspectratio;

    void initEngine();
    void copyEngine(const Engine& source_engine);
    void setEngineParameters();

    Vec3 trace(
        const Vec3 &rayorig,
        const Vec3 &raydir,
        const std::vector<Sphere> &spheres,
        const unsigned &depth);
};

#endif
