// This file is part of Amydrium.
//
// Amydrium is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Amydrium is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Amydrium.  If not, see <http://www.gnu.org/licenses/>.

#ifndef DATA_H
#define DATA_H

#include<iostream>
#include<cmath>

class Vec3{

public:

    float x, y, z;
    
    Vec3();
    Vec3(float x_init);
    Vec3(float x_init,
         float y_init,
         float z_init);
    
    Vec3 operator * (const float &f) const;
    Vec3 operator * (const Vec3 &v) const;
    float dot(const Vec3 &v) const;
    Vec3 operator - (const Vec3 &v) const;
    Vec3 operator + (const Vec3 &v) const;
    Vec3& operator += (const Vec3 &v);
    Vec3& operator *= (const Vec3 &v);
    
    Vec3& operator *= (const float &f);
    
    Vec3 operator - () const;
    
    Vec3& normalize();
    
    float length2() const;
    
    float length() const;
    
    //friend std::ostream & operator << (std::ostream &os, const Vec3 &v);
};


class Sphere {
    
public:

    Vec3 center;                      /// position of the sphere
    float radius, radius2;            /// sphere radius and radius^2
    Vec3 surfaceColor, emissionColor; /// surface color and emission (light)
    float transparency, reflection;   /// surface transparency and reflectivity
    
    Sphere(const Vec3 &c,  const float &r,
           const Vec3 &sc, const float &refl = 0,
           const float &transp = 0, const Vec3 &ec = 0
           );
    
    bool intersect(const Vec3 &rayorig,
                   const Vec3 &raydir,
                   float &t0,
                   float &t1) const;
};

#endif
