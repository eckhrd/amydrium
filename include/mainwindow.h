// This file is part of Amydrium.
//
// Amydrium is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Amydrium is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Amydrium.  If not, see <http://www.gnu.org/licenses/>.

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QGraphicsScene>
#include "data.h"
#include "engine.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow {

    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);

    ~MainWindow();

private slots:
    void on_renderButton_clicked();

    void on_transparencyCheckBox_clicked();

    void on_reflectivityCheckBox_clicked();

    void on_spinBox_valueChanged(int);

private:
    Ui::MainWindow *ui;

    QImage *q_image;

    QGraphicsScene *q_scene;

    std::vector<Sphere> scene;

    Engine engine;

    void drawImage(Vec3*);

};

#endif // MAINWINDOW_H
