# Amydrium
Amydrium is a ray tracer based on the [*Scratchapixel* example
code](https://www.scratchapixel.com/code.php?id=3&origin=/lessons/3d-basic-rendering/introduction-to-ray-tracing). This project served as a sandbox to familiarize myself with C++ and GUI Programming. I modularized the original C++ code and added a GUI using the Qt framework. 

![](./Amydrium.png)

## Compilation

		$ qmake
		$ make
