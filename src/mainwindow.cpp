// This file is part of Amydrium.
//
// Amydrium is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Amydrium is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Amydrium.  If not, see <http://www.gnu.org/licenses/>.

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "data.h"
#include "engine.h"
#include <iostream>
#include <string>
#include <ctime>
#include <vector>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow) {

    ui->setupUi(this);

    q_image = new QImage(640, 480, QImage::Format_RGB32);
    q_scene = new QGraphicsScene( this );

    q_scene->addPixmap( QPixmap::fromImage(*q_image) );
    ui->graphicsView->setScene(q_scene);

    // position, radius, surface color, reflectivity, transparency, emission color
    scene.push_back(Sphere(Vec3( 0.0, -10004, -20), 10000, Vec3(0.20, 0.20, 0.20), 0, 0.0));
    scene.push_back(Sphere(Vec3( 0.0,      0, -20),     4, Vec3(1.00, 0.32, 0.36), 1, 0.5));
    scene.push_back(Sphere(Vec3( 5.0,     -1, -15),     2, Vec3(0.90, 0.76, 0.46), 1, 0.0));
    scene.push_back(Sphere(Vec3( 5.0,      0, -25),     3, Vec3(0.65, 0.77, 0.97), 1, 0.0));
    scene.push_back(Sphere(Vec3(-5.5,      0, -15),     3, Vec3(0.90, 0.90, 0.90), 1, 0.0));

    // light
    scene.push_back(Sphere(Vec3( 0.0,     20, -30),     3, Vec3(0.00, 0.00, 0.00), 0, 0.0, Vec3(3)));

    ui->statusBar->showMessage("Scene initialized.");

    engine.setTransparency( ui->transparencyCheckBox->isChecked() );
    engine.setReflectivity( ui->reflectivityCheckBox->isChecked() );
    engine.setRecursions(   ui->spinBox->value() );

    ui->statusBar->showMessage(ui->statusBar->currentMessage() + " Engine initialized.");

}

MainWindow::~MainWindow() {

    delete ui;
    delete q_image;
    delete q_scene;

}

void MainWindow::on_renderButton_clicked() {

    float cpu_time = engine.render(scene);

    QString cpu_time_message = "CPU time: " + QString::number(cpu_time ) + " seconds";
    ui->statusBar->showMessage( cpu_time_message );

    Vec3* im = engine.getImage();
    drawImage(im);

//    engine.saveImage();

}

void MainWindow::drawImage(Vec3* im) {


    for (int j = 0; j < 480; j++) {
        for (int i = 0; i < 640; i++) {

            int k = i + j*640;

            q_image->setPixel(i, j, qRgb(
                (unsigned char) (std::min(float(1), im[k].x) * 255),
                (unsigned char) (std::min(float(1), im[k].y) * 255),
                (unsigned char) (std::min(float(1), im[k].z) * 255))
            );

        }
    }

    QPixmap pixmap = QPixmap::fromImage(*q_image);

    delete q_image;
    delete q_scene;

    q_image = new QImage(640, 480, QImage::Format_RGB32);
    q_scene = new QGraphicsScene( this );

    q_scene->addPixmap( pixmap );
    ui->graphicsView->setScene(q_scene);

}

void MainWindow::on_transparencyCheckBox_clicked() {
    engine.setTransparency(ui->transparencyCheckBox->isChecked());

    QString message = "Transparency is: " +  QString::number(engine.getTransparency());
    ui->statusBar->showMessage( message );
}

void MainWindow::on_reflectivityCheckBox_clicked() {
    engine.setReflectivity(ui->reflectivityCheckBox->isChecked());

    QString message = "Reflectivity is: " +  QString::number(engine.getReflectivity());
    ui->statusBar->showMessage( message );
}

void MainWindow::on_spinBox_valueChanged(int value)
{
    engine.setRecursions(value);
    QString message = "Recursion depth is: " +  QString::number(engine.getRecursions());
    ui->statusBar->showMessage( message );
}
