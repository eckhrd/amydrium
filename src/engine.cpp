// This file is part of Amydrium.
//
// Amydrium is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Amydrium is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Amydrium.  If not, see <http://www.gnu.org/licenses/>.

#include <iostream>
#include <fstream>
#include <vector>
#include "data.h"
#include "engine.h"

Engine::Engine(bool refl,
               bool trans,
               unsigned rec) :
               reflectivity(refl),
               transparency(trans),
               recursions(rec) {

    width  = 640;
    height = 480;
    fov    = 30;

    setEngineParameters();

    image = new Vec3[width * height];

}

Engine::Engine(const Engine& source_engine) {

    std::cout << "Copy constructor called." << std::endl;

    copyEngine(source_engine);

    setEngineParameters();

    image = new Vec3[width*height];
    std::copy(source_engine.image,
              source_engine.image + (width*height),
              this->image);

}

Engine& Engine::operator = (const Engine& source_engine) {

    if (this != &source_engine) {

        // Releasing original image data because original and source_engine width/height
        // my differ.
        delete [] image;

        copyEngine(source_engine);

        setEngineParameters();

        image = new Vec3[width * height];
        std::copy(source_engine.image,
                  source_engine.image + (width*height),
                  this->image);

    }

    return *this;

}

void Engine::copyEngine(const Engine& source_engine) {

        reflectivity = source_engine.reflectivity;
        transparency = source_engine.transparency;
        recursions   = source_engine.recursions;

        width  = source_engine.width;
        height = source_engine.height;
        fov    = source_engine.fov;

}


void Engine::setEngineParameters() {

    invWidth    = 1 / float(width);
    invHeight   = 1 / float(height);
    aspectratio = width / float(height);
    angle       = tan(M_PI * 0.5 * fov / 180.);

    pixel = NULL;

}

Engine::~Engine() {

    delete [] image;

}


bool Engine::getTransparency(void) {
    return transparency;
}

void Engine::setTransparency(bool status) {
    transparency = status;
}

void Engine::setReflectivity(bool status) {
    reflectivity = status;
}

bool Engine::getReflectivity(void) {
    return reflectivity;
}

void Engine::setRecursions(unsigned number_of_recursions) {
    recursions = number_of_recursions;
}

unsigned Engine::getRecursions(void) {
    return recursions;
}

// Main rendering function. We compute a camera ray for each pixel of the image
// trace it and return a color. If the ray hits a sphere, we return the color of the
// sphere at the intersection point, else we return the background color.
float Engine::render(const std::vector<Sphere> &scene) {

    clock_t begin = clock();


// #pragma omp parallel for
    for (unsigned y = 0; y < height; ++y) {

        for (unsigned x = 0; x < width; ++x) {
            
            float xx = (2 * ((x + 0.5) * invWidth) - 1)  * angle * aspectratio;
            float yy = (1 - 2 * ((y + 0.5) * invHeight)) * angle;
            
            Vec3*p = image + y*width + x;
            Vec3 raydir(xx, yy, -1);
            raydir.normalize();

//            *p = returnVec(Vec3(0), raydir);
            *p =  trace(Vec3(0), raydir, scene, 0);
        }
    
    }


    return float(clock() - begin) / CLOCKS_PER_SEC ;

}

// Save result to a PPM image (keep these flags if you compile under Windows)
void Engine::saveImage() {
    
    std::ofstream ofs("./untitled.ppm", std::ios::out | std::ios::binary);
    ofs << "P6\n" << width << " " << height << "\n255\n";
    
    for (unsigned i = 0; i < width * height; ++i) {
        ofs << (unsigned char)(std::min(float(1), image[i].x) * 255) <<
               (unsigned char)(std::min(float(1), image[i].y) * 255) <<
               (unsigned char)(std::min(float(1), image[i].z) * 255);

    }

    ofs.close();
    std::cout << "Image saved." << std::endl;

}

//Vec3 Engine::returnVec(const Vec3& offset, const Vec3& raydir) {
//    return offset + raydir + Vec3(float(omp_get_thread_num()) / omp_get_num_threads());
//}

Vec3* Engine::getImage() {

    return image;

}


// This is the main trace function. It takes a ray as argument (defined by its origin
// and direction). We test if this ray intersects any of the geometry in the scene.
// If the ray intersects an object, we compute the intersection point, the normal
// at the intersection point, and shade this point using this information.
// Shading depends on the surface property (is it transparent, reflective, diffuse).
// The function returns a color for the ray. If the ray intersects an object that
// is the color of the object at the intersection point, otherwise it returns
// the background color.
Vec3 Engine::trace(

    const Vec3 &rayorig,
    const Vec3 &raydir,
    const std::vector<Sphere> &spheres,
    const unsigned &depth) {
    
    //if (raydir.length() != 1) std::cerr << "Error " << raydir << std::endl;
    float tnear = INFINITY;
    const Sphere* sphere = NULL;

    // find intersection of this ray with the sphere in the scene
    for (unsigned i = 0; i < spheres.size(); ++i) {
        
        float t0 = INFINITY, t1 = INFINITY;

        if (spheres[i].intersect(rayorig, raydir, t0, t1)) {
            
            if (t0 < 0) t0 = t1;
            
            if (t0 < tnear) {
                
                tnear = t0;
                sphere = &spheres[i];
            
            }
        
        }
        
    }
    
    // if there's no intersection return black or background color
    if (!sphere) return Vec3(2);
    
    Vec3 surfaceColor = 0; // color of the ray/surfaceof the object intersected by the ray
    Vec3 phit         = rayorig + raydir * tnear; // point of intersection
    Vec3 nhit         = phit - sphere->center; // normal at the intersection point
    nhit.normalize(); // normalize normal direction
    
    // If the normal and the view direction are not opposite to each other
    // reverse the normal direction. That also means we are inside the sphere so set
    // the inside bool to true. Finally reverse the sign of IdotN which we want
    // positive.
    float bias = 1e-4; // add some bias to the point from which we will be tracing
    bool inside = false;
    if (raydir.dot(nhit) > 0) nhit = -nhit, inside = true;

    if (((transparency && sphere->transparency > 0 ) ||
         (reflectivity && sphere->reflection > 0   ) ) && depth < recursions) {
        
        float facingratio = -raydir.dot(nhit);
        
        // change the mix value to tweak the effect
        float fresneleffect = mix(pow(1 - facingratio, 3), 1, 0.1);
        
        // compute reflection direction (not need to normalize because all vectors
        // are already normalized)
        Vec3 refldir = raydir - nhit * 2 * raydir.dot(nhit);
        refldir.normalize();
        
        Vec3 reflection = trace(phit + nhit * bias, refldir, spheres, depth + 1);
        Vec3 refraction = 0;
        
        // if the sphere is also transparent compute refraction ray (transmission)
        if (transparency && sphere->transparency) {
            
            float ior  = 1.1;
            float eta  = (inside) ? ior : 1 / ior; // are we inside or outside the surface?
            float cosi = -nhit.dot(raydir);
            float k    = 1 - eta * eta * (1 - cosi * cosi);
            
            Vec3 refrdir = raydir * eta + nhit * (eta *  cosi - sqrt(k));
            refrdir.normalize();
            refraction = trace(phit - nhit * bias, refrdir, spheres, depth + 1);
        
        }
        
        // the result is a mix of reflection and refraction (if the sphere is transparent)
        surfaceColor = (
            reflection * fresneleffect +
            refraction * (1 - fresneleffect) * sphere->transparency) * sphere->surfaceColor;
    
    } else {
        
        // it's a diffuse object, no need to raytrace any further
        for (unsigned i = 0; i < spheres.size(); ++i) {
            
            if (spheres[i].emissionColor.x > 0) {
                
                // this is a light
                Vec3 transmission   = 1;
                Vec3 lightDirection = spheres[i].center - phit;
                lightDirection.normalize();
                
                for (unsigned j = 0; j < spheres.size(); ++j) {
                    
                    if (i != j) {
                        
                        float t0, t1;
                        if (spheres[j].intersect(phit + nhit * bias, lightDirection, t0, t1)) {
                            
                            transmission = 0;
                            break;
                        
                        }
                    
                    }
                
                }
                
                surfaceColor +=
                    sphere->surfaceColor * transmission *
                    std::max(float(0), nhit.dot(lightDirection)) * spheres[i].emissionColor;
            
            }
        
        }
   
    }
    
    return surfaceColor + sphere->emissionColor;

}
