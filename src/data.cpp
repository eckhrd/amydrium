// This file is part of Amydrium.
//
// Amydrium is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Amydrium is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Amydrium.  If not, see <http://www.gnu.org/licenses/>.

#include <iostream>
#include <cmath>
#include "data.h"

Vec3::Vec3() : x(1.0), y(1.0), z(1.0) {
    }
    
Vec3::Vec3(float x_init) : x(x_init), y(x_init), z(x_init) {
    }
    
Vec3::Vec3(float x_init,
         float y_init,
         float z_init) : x(x_init), y(y_init), z(z_init) {
    }
    
Vec3 Vec3::operator * (const float &f) const {
        return Vec3(x * f, y * f, z * f);
    }
    
Vec3 Vec3::operator * (const Vec3 &v) const {
        return Vec3(x * v.x, y * v.y, z * v.z);
    }
    
float Vec3::dot(const Vec3 &v) const {
        return x * v.x + y * v.y + z * v.z;
    }
    
Vec3 Vec3::operator - (const Vec3 &v) const {
        return Vec3(x - v.x, y - v.y, z - v.z);
    }
    
Vec3 Vec3::operator + (const Vec3 &v) const {
        return Vec3(x + v.x, y + v.y, z + v.z);
    }
    
Vec3& Vec3::operator += (const Vec3 &v) {
        x += v.x, y += v.y, z += v.z;
        return *this;
    }
    
Vec3& Vec3::operator *= (const Vec3 &v) {
        x *= v.x, y *= v.y, z *= v.z;
        return *this;
    }
    
Vec3& Vec3::operator *= (const float &f) {
        x *= f, y *= f, z *= f;
        return *this;
    }
    
Vec3 Vec3::operator - () const {
        return Vec3(-x, -y, -z);
    }
    
Vec3& Vec3::normalize() {
        float norm2 = length2();
        if (norm2 > 0) *this *= 1 / sqrt(norm2); // Optimization potential, remove if.
        return *this;
    }
    
float Vec3::length2() const {
        return x * x + y * y + z * z;
    }
    
float Vec3::length() const {
        return sqrt( length2() );
    }
    
/*
friend std::ostream & operator << (std::ostream &os, const Vec3 &v)
    {
        os << "[" << v.x << " " << v.y << " " << v.z << "]";
        return os;
    }
*/


Sphere::Sphere(const Vec3 &c,  const float &r,
               const Vec3 &sc, const float &refl,
               const float &transp, const Vec3 &ec
              ) : center(c), radius(r), radius2(r * r),
                  surfaceColor(sc), emissionColor(ec),
                  transparency(transp), reflection(refl) {
};

// Compute a ray-sphere intersection using the geometric solution
bool Sphere::intersect(const Vec3 &rayorig,
                       const Vec3 &raydir,
                       float &t0,
                       float &t1) const {
    
    Vec3 l = center - rayorig;
    
    float tca = l.dot(raydir);
    if (tca < 0) return false;
    
    float d2 = l.dot(l) - tca * tca;
    if (d2 > radius2) return false;
    
    float thc = sqrt(radius2 - d2);
    t0 = tca - thc;
    t1 = tca + thc;
    
    return true;
}
